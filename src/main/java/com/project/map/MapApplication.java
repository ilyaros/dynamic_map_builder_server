package com.project.map;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
@Slf4j
public class MapApplication {
    @Autowired
    LinksStorage linksStorage;
    @Autowired
    Configuration configuration;
    static boolean isGuStartEnable = false;

    public static void main(String[] args) {
        SpringApplication.run(MapApplication.class, args);
        if (isGuStartEnable) {
            log.warn("Debug gui is ON");
            GuiService.startGui();
        }else log.info("Debug gui is OFF");

    }

    @PostConstruct
    void guiInit() {
        GuiService.setLinksStorage(linksStorage);
        GuiService.setConfiguration(configuration);
        isGuStartEnable = configuration.getIsDebugGuiEnable();
    }
}
