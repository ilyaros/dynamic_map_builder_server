package com.project.map;

import com.project.map.model.Coordinates;
import com.project.map.model.Link;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Pair;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
public class GuiService extends Application {

    long lastDragAndDropEvent = System.currentTimeMillis();
    private static final int DRAG_AND_DROP_DELAY = 100;

    private static Canvas staticMap;
    private static Canvas dynamicMap = new Canvas(750, 750);
    private static Scene scene;
    private List<Coordinates> currentRouteCoordinates = new ArrayList<>();

    @Setter
    private static LinksStorage linksStorage;

    @Setter
    private static Configuration configuration;

    public static void startGui() {
        Application.launch();
    }

    @Override
    public void start(Stage stage) {
        stage.setResizable(false);
        dynamicMap = createDynamicMap();
        staticMap = createStaticMap();
        VBox vBox = buttonBoxInit();
        HBox hbox = new HBox(10, dynamicMap, staticMap, vBox);

        Group root = new Group();
        scene = new Scene(root, 1650, 750);
        root.getChildren().add(hbox);
        stage.setScene(scene);
        stage.show();
    }

    private VBox buttonBoxInit() {
        Button buttonRefresh = new Button("Refresh");
        buttonRefresh.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                log.debug("Pressed <Refresh> button");
                clearAndPrintLinks();
            }
        });
        Button buttonOptimization = new Button("Optimization");

        return new VBox(20, buttonRefresh, buttonOptimization);
    }

    private Canvas createDynamicMap() {
        clearAndPrintLinks();
        return dynamicMap;
    }

    private void printGrid(Canvas canvas) {
        double width = canvas.getWidth();
        double height = canvas.getHeight();
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setStroke(Color.GRAY);
        graphicsContext.setLineWidth(1);

        // горизонтальные линии сетки координат
        for (int i = 0; i < width; i += configuration.getGridStep()) {
            graphicsContext.strokeLine(i, 0, i, height);
        }
        // вертикальные линии сетки координат
        for (int i = 0; i < height; i += configuration.getGridStep()) {
            graphicsContext.strokeLine(0, i, width, i);
        }
    }

    private Canvas createStaticMap() {
        Canvas staticMap = new Canvas(750, 750);
        GraphicsContext graphicsContext = staticMap.getGraphicsContext2D();

        graphicsContext.drawImage(new Image("map.jpg"), 0, 0, staticMap.getWidth(), staticMap.getHeight());

        addDragAndDropHandler(staticMap);
        changeCursor(staticMap);
        addReleaseButtonHandler(staticMap);

        return staticMap;
    }

    private void addDragAndDropHandler(Canvas canvas) {
        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (System.currentTimeMillis() > lastDragAndDropEvent + DRAG_AND_DROP_DELAY) {
                    Coordinates curCoord = new Coordinates(mouseEvent.getX(), mouseEvent.getY());
                    currentRouteCoordinates.add(curCoord);
                    log.debug("Mouse dragged {}", curCoord);
                    dynamicMap.getGraphicsContext2D().setFill(Color.rgb(0, 0, 0));
                    dynamicMap.getGraphicsContext2D().fillOval(mouseEvent.getX(), mouseEvent.getY(), 4, 4);
                    lastDragAndDropEvent = System.currentTimeMillis();
                }
            }
        });
    }

    private void addReleaseButtonHandler(Canvas canvas) {
        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                log.debug("button release");
                clearAndPrintLinks();

                List<Link> newLinks = linksStorage.addNewLinksFromPoints(currentRouteCoordinates);
                currentRouteCoordinates = new ArrayList<>();
                printLinks(dynamicMap, newLinks, Color.RED);
            }
        });
    }

    private void clearAndPrintLinks() {
        GraphicsContext graphicsContext = dynamicMap.getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, dynamicMap.getWidth(), dynamicMap.getHeight());
        printGrid(dynamicMap);
        printLinks(dynamicMap, linksStorage.getLinkList(), Color.GREEN);
    }

    private void printLinks(Canvas canvas, List<Link> links, Color color) {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setStroke(color);
        graphicsContext.setLineWidth(3);

        if (!links.isEmpty()) {
            for (Link link : links) {
                Coordinates from = link.getFirst();
                Coordinates to = link.getSecond();
                graphicsContext.strokeLine(from.getLongitude(), from.getLatitude(),
                        to.getLongitude(), to.getLatitude());
                graphicsContext.fillOval(from.getLongitude(), from.getLatitude(), 6, 6);
            }
            Coordinates lastPoint = links.get(links.size() - 1).getSecond();
            graphicsContext.fillOval(lastPoint.getLongitude(), lastPoint.getLatitude(), 6, 6);
        }else {
            log.error("Links list is empty");
        }
    }

    private void changeCursor(Canvas canvas) {
        canvas.addEventHandler(MouseEvent.MOUSE_ENTERED_TARGET, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                log.debug("Mouse ENTERED_TARGET X={} Y={}", mouseEvent.getX(), mouseEvent.getY());
                ImageCursor cursor = new ImageCursor(new Image("walkCursor.gif"
                ));
                scene.setCursor(cursor);
            }
        });
        canvas.addEventHandler(MouseEvent.MOUSE_EXITED_TARGET, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                log.debug("Mouse ENTERED_TARGET X={} Y={}", mouseEvent.getX(), mouseEvent.getY());
                Cursor cursor = Cursor.DEFAULT;
                scene.setCursor(cursor);
            }
        });
    }
}
