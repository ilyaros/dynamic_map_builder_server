package com.project.map;

import com.project.map.model.Coordinates;
import com.project.map.model.Link;
import lombok.Getter;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Getter
public class LinksStorage {

    @Autowired
    Configuration configuration;

    @Autowired
    LinksService linksService;

    private final List<Link> linkList = new ArrayList<>();

    public List<Link> addNewLinksFromPoints(List<Coordinates> originCoordinates) {

        List<Coordinates> mappedOnGrid = linksService.mapRouteAndGrid(originCoordinates);

        List<Link> newLinks = new ArrayList<>(mappedOnGrid.size());

        for (int i = 1; i < mappedOnGrid.size(); i++) {
            newLinks.add(new Link(mappedOnGrid.get(i - 1), mappedOnGrid.get(i)));
        }

        linkList.addAll(newLinks);
        return newLinks;
    }

}
