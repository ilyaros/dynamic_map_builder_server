package com.project.map.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Link {
    private Coordinates first;
    private Coordinates second;
    private double weight;

    public Link(Coordinates first, Coordinates second) {
       this.first = first;
       this.second = second;
       this.weight = 1D;
    }
}
