package com.project.map.model.dto;

import com.project.map.model.Coordinates;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddRouteRequestDto {
    List<Coordinates> items;
}
