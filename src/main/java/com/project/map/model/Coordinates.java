package com.project.map.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Coordinates {
    // Долгота (X)
    private Double longitude;

    // Широта (Y)
    private Double latitude;
}
