package com.project.map;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@ToString
public class Configuration {
    @Value("${spring.application.name}")
    private String appName;

    @Value("${map.grid.step}")
    private double gridStep;

    @Value("${gui.isEnable}")
    private Boolean isDebugGuiEnable;

}
