package com.project.map.controllers;

import com.project.map.Configuration;
import com.project.map.GuiService;
import com.project.map.LinksStorage;
import com.project.map.model.Coordinates;
import com.project.map.model.dto.AddRouteRequestDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
public class RouteController {

    private final Configuration configuration;
    private final LinksStorage linksStorage;

    @RequestMapping("/")
    public String index() {
        return "hello from app <" + configuration.getAppName() + ">";
    }

    @PostMapping("/route/add")
    public void routeAdd(@RequestBody AddRouteRequestDto routeToAdd) {
        List<Coordinates> items = routeToAdd.getItems();
        if (CollectionUtils.isEmpty(items)) throw new RuntimeException("Body must not be empty");
        if (items.size() < 2) throw new RuntimeException("Must be more then one points of the route");
        log.info("New route with {} points received", items.size());

        linksStorage.addNewLinksFromPoints(items);
    }
}
