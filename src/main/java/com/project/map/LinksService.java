package com.project.map;

import com.project.map.model.Coordinates;
import org.apache.commons.math3.geometry.euclidean.twod.Line;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.util.FastMath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class LinksService {

    private static final Integer MAX_LINE_COUNT = 1000;
    private final List<Line> verticalLines = new ArrayList<>();
    private final List<Line> horizontalLines = new ArrayList<>();
    @Autowired
    private final Configuration configuration;

    public LinksService(Configuration configuration) {
        this.configuration = configuration;

        for (int i = 0; i < MAX_LINE_COUNT; i++) {
            Vector2D point = new Vector2D(i * configuration.getGridStep(), 0);
            verticalLines.add(new Line(point, FastMath.PI/2));
        }
        for (int i = 0; i < MAX_LINE_COUNT; i++) {
            Vector2D point = new Vector2D(0, i * configuration.getGridStep());
            horizontalLines.add(new Line(point, 0));
        }
    }


    public List<Coordinates> mapRouteAndGrid(List<Coordinates> originRoute) {
        List<Coordinates> pointsOnGrid = new ArrayList<>();

        if (isPointOnGrid(originRoute.get(0))) pointsOnGrid.add(originRoute.get(0));

        for (int i = 1; i < originRoute.size(); i++) {
            Vector2D privPoint = getVector2DFromCoordinates(originRoute.get(i - 1));
            Vector2D curPoint = getVector2DFromCoordinates(originRoute.get(i));
            Line line = new Line(privPoint, curPoint);
            // Все пересечения с горизонтальной сеткой
            List<Vector2D> intersectionWithHorizontalGrid = horizontalLines.stream()
                    .map(l -> l.intersection(line))
                    .filter(Objects::nonNull)
                    .filter(p -> (p.getY() >= privPoint.getY() && p.getY() <= curPoint.getY())
                            || (p.getY() <= privPoint.getY() && p.getY() >= curPoint.getY()))
                    .collect(Collectors.toList());

            // Все пересечения с вертикальной сеткой
            List<Vector2D> intersectionWithVerticalGrid = verticalLines.stream()
                    .map(l -> l.intersection(line))
                    .filter(Objects::nonNull)
                    .filter(p -> (p.getX() >= privPoint.getX() && p.getX() <= curPoint.getX())
                            || (p.getX() <= privPoint.getX() && p.getX() >= curPoint.getX()))
                    .collect(Collectors.toList());

            List<Vector2D> allIntersectionWithGrid = new ArrayList<>(intersectionWithHorizontalGrid);
            allIntersectionWithGrid.addAll(intersectionWithVerticalGrid);

            List<Coordinates> crossesForOneLink = allIntersectionWithGrid.stream()
                    .sorted(Comparator.comparingDouble(o -> o.distance(privPoint)))
                    .map(p -> new Coordinates(p.getX(), p.getY()))
                    .collect(Collectors.toList());
            pointsOnGrid.addAll(crossesForOneLink);
        }

        return pointsOnGrid;
    }

    private boolean isPointOnGrid(Coordinates point) {
        Double gridStep = configuration.getGridStep();
        return (point.getLatitude() % gridStep == 0 || point.getLongitude() % gridStep == 0);
    }

    private Vector2D getVector2DFromCoordinates(Coordinates coordinate) {
        return new Vector2D(coordinate.getLongitude(), coordinate.getLatitude());
    }

}
